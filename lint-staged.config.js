//lint-staged.config.js
module.exports = {
  // Type check TypeScript files
  '**/*.(ts)': () => 'tsc --noEmit',

  // Lint then format TypeScript and JavaScript files
  '**/*.(ts|js)': (filenames) => [`eslint --fix ${filenames.join(' ')}`, `prettier --write ${filenames.join(' ')}`],

  // Format MarkDown and JSON
  '**/*.(md|json)': (filenames) => `prettier --write ${filenames.join(' ')}`,
};
